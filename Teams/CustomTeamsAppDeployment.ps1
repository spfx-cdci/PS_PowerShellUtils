###############################################################################
# Teams App Deployment Script
###############################################################################
# Parameters         
# > scopes (string)    
###############################################################################
# Example 
# ./CustomTeamsAppDeployment.ps1 -scopes "Groups.ReadWrite.All"
###############################################################################
# Requires the manifest folder path due to Teams App dev flavors (SPA, SPFx...)
###############################################################################

# Script command lines - Target Site Collection with an app catalog/do you want to install the app
param(
    [Parameter(Mandatory = $true)][string]$scopes
)

# External Variables - To get the username
$WorkingDir = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
. $WorkingDir\Helpers\Variables.ps1
. $WorkingDir\Helpers\Functions.ps1

Write-Host " "
Write-Host "#########################################"
Write-Host "# --- Teams Build and Deploy script --- #"
Write-Host "#########################################"
Write-Host " "

# 0 - Getting manifest folder path & checking if the manifest folder is here
If ([string]::IsNullOrEmpty($ManifestPath)) {
    Write-Host "# WARNING - You did not provide a manifest folder path. Exiting..." -ForegroundColor DarkYellow
    Write-Host "# Info - Please modify the value of the ManifestPath in the Variables.ps1 file." -ForegroundColor DarkCyan
    exit
}
Else {
    $IsTheManifestPresent = LookForManifestFile -folderPath $ManifestPath -pattern "manifest.json" -excludedPattern "*.zip"
    If ($IsTheManifestPresent) {
        Write-Host "# Step - The manifest.json was successfully found!" -ForegroundColor green
    }
    Else {
        Write-Host "# Error - Exiting.The manifest.json was not found." -ForegroundColor DarkYellow
    }
} 

# 0 - Reading current SPFx project package configuration to prevent known errors
$ReadSPFxSolutionSettings = Get-Content -Raw -Path $ManifestPath\manifest.json | ConvertFrom-Json

# 1 - Checks if folder exists
Write-Host " "
Write-Host "#### Deployment Folder creation ####"
Write-Host " "

CreateFolderIfNotExist $DeploymentLogsFolder "deployment"

#2 - Zipping the ressources
Write-Host " "
Write-Host "#### Creating the manifest zip ####"
Write-Host " "
$IsTheZipPresent = LookForManifestFile -folderPath $ManifestPath -pattern "*.zip" 

if ($IsTheZipPresent) {
    Write-Host "# Step - A previous zip exists. Replacing it..." -ForegroundColor green
    Remove-Item $ManifestPath\*.zip
}
Compress-Archive -Path $ManifestPath\* -DestinationPath $ManifestPath\manifest.zip -Force

# 4 - Connect to the SharePoint site using PnPConnection.ps1
Write-Host " "
Write-Host "#### PnP Connection ####"
Write-Host " "
$CurrentConnectionInstance = . $WorkingDir\Helpers\PnPConnection.ps1 -scopes $scopes

# 5 - Checking if its not already on
Write-Host " "
Write-Host "#### Checking is already existing ####"
Write-Host " "
$ExistingApp = CheckIfTeamsAppExists -AppShortName $readSPFxSolutionSettings.name.short

# 5 - Install the app
Write-Host " "
Write-Host "#### Teams App Deployment/Update ####"
Write-Host " "

If ($ExistingApp) {
    Try {
        $AppUpdate = Update-PnPTeamsApp -Identity $ExistingApp."ID" -Path $ManifestPath\manifest.zip 
        Write-Host "# Step - The App was updated to version:" $readSPFxSolutionSettings.version -ForegroundColor green
    }
    Catch [Exception] {
        $ErrorMessage = $_.Exception.Message
        Write-Error "# Error - While updating the Teams App of ID" $ExistingApp."ID" "-" $ErrorMessage
        Write-Host "# Info - Make sure to update the manifest version or it may cause an error." -ForegroundColor DarkCyan
        exit
    }
}
Else {
    Try {
        Write-Host "# Step - Installing the application of version:" $readSPFxSolutionSettings.version -ForegroundColor green
        New-PnPTeamsApp -Path $ManifestPath\manifest.zip
    }
    Catch [Exception] {
        $ErrorMessage = $_.Exception.Message
        Write-Error "# Error - While adding the Teams App: $ErrorMessage"
        Write-Host "# Info - Make sure to update the manifest version or it may cause an error." -ForegroundColor DarkCyan
        exit
    }
}


# 6 - Checks if it is really installed
Write-Host " "
Write-Host "#### Checking is the app is installed ####"
Write-Host " "
$IsInstalled = CheckIfTeamsAppExists -AppShortName $readSPFxSolutionSettings.name.short

# 7 - Closing
Write-Host " "
Write-Host "#### Closing connection ####"
Write-Host " "
Write-Host "# Step - Closing connection ..." -ForegroundColor green
If ($CurrentConnectionInstance) {
    Disconnect-PnPOnline
}
Write-Host " "
Write-Host "End of script without errors." -ForegroundColor DarkCyan
