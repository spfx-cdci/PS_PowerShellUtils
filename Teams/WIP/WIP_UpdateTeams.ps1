Add-PnPStoredCredential -Name https://m365x649892.sharepoint.com -Username admin@M365x649892.onmicrosoft.com -Password (ConvertTo-SecureString -String "4XP51Dmx3U" -AsPlainText -Force)

# Connect with the Microsoft Graph via PnP PowerShell
# https://docs.microsoft.com/en-us/powershell/sharepoint/sharepoint-pnp/sharepoint-pnp-cmdlets?view=sharepoint-ps
Connect-PnPOnline "https://m365x649892.sharepoint.com" -Scopes "Group.ReadWrite.All"
$accesstoken = Get-PnPAccessToken
 
# Example of json for POST https://graph.microsoft.com/v1.0/groups
# https://docs.microsoft.com/en-us/graph/api/group-post-groups?view=graph-rest-1.0

# Liste tous les Teams
$apiUrl = 'https://graph.microsoft.com/beta/Groups/'
$myProfile = Invoke-RestMethod -Headers @{Authorization = "Bearer $accessToken"} -Uri $apiUrl -Method Get
$GroupsList = $myprofile | select-object Value
$Groups = $GroupsList.Value | Select-Object Id

foreach($Group in $Groups){
   $GroupId = $Group.Id
   $apiUrl2 = "https://graph.microsoft.com/beta/Groups/$Groupid/"
   $GroupsData = Invoke-RestMethod -Headers @{Authorization = "Bearer $accessToken"} -Uri $apiUrl2 -Method Get
   $NameType = $GroupsData | Select-Object id,resourceProvisioningOptions,DisplayName
   If($NameType.DisplayName -eq 'Sample Engineering Team'){
       $selectedID = $GroupId
       $GroupId + "Is selected"
   }
}

# $response = Invoke-RestMethod -Uri https://graph.microsoft.com/beta/groups?$select=id,resourceProvisioningOptions,displayName -ContentType "application/json" -Headers @{Authorization = "Bearer $accesstoken"} -Method Get
# Write-Host $response
# Foreach($element in $response){
#     Write-Host $element
# }

$updateTeam = @'
{
    "displayName": "Sample Engineering Team Mod",
    "description": "This is a sample engineering team, used to showcase the range of properties supported by this API",
    "channels": [
        {
            "displayName": "Announcements2",
            "isFavoriteByDefault": true,
            "description": "This is a sample announcements channel that is favorited by default. Use this channel to make important team, product, and service announcements."
        },
        {
            "displayName": "Training",
            "isFavoriteByDefault": true,
            "description": "This is a sample training channel, that is favorited by default, and contains an example of pinned website and YouTube tabs.",
            "tabs": [
                {
                    "teamsApp@odata.bind": "https://graph.microsoft.com/v1.0/appCatalogs/teamsApps('com.microsoft.teamspace.tab.web')",
                    "name": "A Pinned Website",
                    "configuration": {
                        "contentUrl": "https://docs.microsoft.com/en-us/microsoftteams/microsoft-teams"
                    }
                },
                {
                    "teamsApp@odata.bind": "https://graph.microsoft.com/v1.0/appCatalogs/teamsApps('com.microsoft.teamspace.tab.youtube')",
                    "name": "A Pinned YouTube Video",
                    "configuration": {
                        "contentUrl": "https://tabs.teams.microsoft.com/Youtube/Home/YoutubeTab?videoId=X8krAMdGvCQ",
                        "websiteUrl": "https://www.youtube.com/watch?v=X8krAMdGvCQ"
                    }
                }
            ]
        },
        {
            "displayName": "Planning",
            "description": "This is a sample of a channel that is not favorited by default, these channels will appear in the more channels overflow menu.",
            "isFavoriteByDefault": false
        },
        {
            "displayName": "Issues and Feedback",
            "description": "This is a sample of a channel that is not favorited by default, these channels will appear in the more channels overflow menu."
        }
    ],
    "memberSettings": {
        "allowCreateUpdateChannels": true,
        "allowDeleteChannels": true,
        "allowAddRemoveApps": true,
        "allowCreateUpdateRemoveTabs": true,
        "allowCreateUpdateRemoveConnectors": true
    },
    "guestSettings": {
        "allowCreateUpdateChannels": false,
        "allowDeleteChannels": false
    },
    "funSettings": {
        "allowGiphy": true,
        "giphyContentRating": "Moderate",
        "allowStickersAndMemes": true,
        "allowCustomMemes": true
    },
    "messagingSettings": {
        "allowUserEditMessages": true,
        "allowUserDeleteMessages": true,
        "allowOwnerDeleteMessages": true,
        "allowTeamMentions": true,
        "allowChannelMentions": true
    },
    "discoverySettings": {
        "showInTeamsSearchAndSuggestions": true
    }
  }
'@
Write-Host $selectedID
$response = Invoke-RestMethod -Uri "https://graph.microsoft.com/beta/teams/$selectedID" -Body $updateTeam -ContentType "application/json" -Headers @{Authorization = "Bearer $accesstoken"} -Method Patch
