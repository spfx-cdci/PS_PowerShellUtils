###########################################################
# PnP Connection Script 
###########################################################
# Parameters         
# > siteUrl (string)   
# > scopes (string)  
###########################################################
# Example 
# ./PnPConnection.ps1 -scopes "Group.ReadWrite.All"
###########################################################
# Uses secured credentials generated on first connection. 
###########################################################

# Command line parameters
# SiteUrl typically for Sharepoint, $scopes when you try to deploy on Teams which requires Groups scope consent
Param (
    [Parameter(Mandatory = $false)][string]$siteUrl,
    [Parameter(Mandatory = $false)][string]$scopes
)

# External Variables - To get the username
$WorkingDir = Split-Path $MyInvocation.MyCommand.Path -Parent
. $WorkingDir\Variables.ps1

Write-Host " "
Write-Host "#########################################"
Write-Host "# --- PnP Connection Instance Start --- #"
Write-Host "#########################################"
Write-Host " "

# 0 - If the credential file doesnt exist, ask the user (Only once).
# NOTE: Teams App deployment will require to consent to the scopes (Pop up)
If (![System.IO.File]::Exists("$WorkingDir/SPSecuredStrings.txt")) {
    . $WorkingDir\GenerateSecuredCredentials.ps1
}
Else {
    # If it exists - Read
    $EncryptedPassword = Get-Content $WorkingDir/SPSecuredStrings.txt | ConvertTo-SecureString
    $Credentials = New-Object System.Management.Automation.PsCredential($ConnectionUsername, $EncryptedPassword)
}

# 1 - Checks if the PnP Module is installed ----
If (Get-Module -ListAvailable -Name "SharePointPnPPowerShell*") {
    Write-Host "# Step - A PnP Powershell module was found. Processing." -ForegroundColor green
}
Else {
    Write-Error "# Error - No PnP Powershell module was found. Exiting."
    exit
}

# 2 - Connecting to the SharePoint site
Write-Host "# Step - Connecting to the SharePoint site as $ConnectionUsername ..." -ForegroundColor green
Write-Host "# Warning - If you want to change the user, please change it in the Variables.ps1 file" -ForegroundColor DarkYellow
Try {
    If ($scopes) {
        # To connect an deploy on teams
        # https://github.com/pnp/PnP-PowerShell/issues/1340
        # "The Connect-PnPOnline will use stored credentials when connecting to a tenant URL 
        # but when the -Scopes is used it will not. This prohibits being able to create Office 365 Groups thru PnP PowerShell thru automation."
        # Tested with "Group.ReadWrite.All" to provision teams apps
        Connect-PnPOnline -Scopes $scopes -Credentials $Credentials
    }
    Else {
        Connect-PnPOnline -Url $siteUrl -Credentials $Credentials
    }
}
Catch [Exception] {
    $ErrorMessage = $_.Exception.Message
    Write-Error "# Error - Connection failed due to the following reason: $ErrorMessage"
}