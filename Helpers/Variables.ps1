###########################################################
# Variables
###########################################################

#######################
# PnP Connection      #
#######################
$ConnectionUsername = "laurent@smeethy.onmicrosoft.com"

#######################
# SPFx and SharePoint #
#######################

# To build and deploy spfx apps
$DeploymentLogsFolder = "./deployment"
$GulpFile = "./gulpfile.js"

# Export to csv vars
$ExportedCSV = "./exportedLists"

# Exported Pages
$ExportedPages = "./exportedPages"

# SharePoint List XML templates
$XMLTemplatesFolder = "./xmlTemplates"

########################
# Teams App Deployment #
########################

$ManifestPath = "C:\Users\Smeethy\Desktop\Work\A3\Repos\sources\POC-TeamsHub\Manifest"

