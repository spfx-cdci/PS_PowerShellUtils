###########################################################
# Set and encrypt credentials not to enter them everytime
###########################################################

# External Variables - To get the username
$WorkingDir = Split-Path $MyInvocation.MyCommand.Path -Parent
. $WorkingDir\Variables.ps1

##########################
# ---- Script start ---- #
##########################

$Credential = Get-Credential -UserName $ConnectionUsername -Message 'Enter Password'
$Credential.Password | ConvertFrom-SecureString | Set-Content $WorkingDir\SPSecuredStrings.txt