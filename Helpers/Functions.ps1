###########################################################
# Support Functions
###########################################################

<#
.SYNOPSIS
# Create the folder if it doesnt exist

.DESCRIPTION
Takes 2 parameters : the path to the hypothetical folder and the folder name

.EXAMPLE
CreateFolderIfNotExist "path/myfolder"

.NOTES
General notes
#>
Function CreateFolderIfNotExist {
    If (Test-Path $args[0]) {
        Write-Host "# Step - Required folder exists. Skipping" $args[1] "folder creation." -ForegroundColor green
    }
    Else {
        Write-Host "# Step - Required folder doesn't exist. Creating" $args[1] "folder..." -ForegroundColor green
        New-Item -Name $args[0] -ItemType directory
    }
}

<#
.SYNOPSIS
# Exports a SharePoint List to CSV

.DESCRIPTION
Long description

.EXAMPLE
ExportList -listName $listName -fields $fields

.NOTES
General notes
#>
Function ExportList {  
    Param (
        [Parameter(Mandatory = $true)][string]$listName,
        [Parameter(Mandatory = $true)][System.String[]] $fields
    )
    Try {  
        Write-Host "# Step - Starting SharePoint list $listName export..." -ForegroundColor green
        # Get all list items and prepare output file 
        $TimeOfExport = $(get-date).ToString("yyyyMMddHHmmss")
        $ListItems = (Get-PnPListItem -List $listName -Fields $fields).FieldValues  
        $OutputFilePath = "$ExportedCSV\" + $listName + "-" + $TimeOfExport + ".csv"  
        
        #Array to Hold List Items
        $Table = @()  
 
        # Loop through the list items  
        Foreach ($ListItem in $ListItems) {  
            $CSVLine = New-Object PSObject              
            $ListItem.GetEnumerator() | Where-Object { $_.Key -in $fields } | ForEach-Object { $CSVLine | Add-Member Noteproperty $_.Key $_.Value }  
            $Table += $CSVLine;  
            $CSVLine = $null;  
        }  
  
        Write-Host "# Step - Creating the resulting CSV..." -ForegroundColor green
        $Table | Export-Csv $OutputFilePath -NoTypeInformation 
    }  
    Catch [Exception] {  
        $ErrorMessage = $_.Exception.Message         
        Write-Host "# Error - While exporting the sharepoint list data: $ErrorMessage" -ForegroundColor Red          
    } 
}  


<#
.SYNOPSIS
#

.DESCRIPTION
Long description

.PARAMETER folderPath
Parameter description

.PARAMETER pattern
Parameter description

.PARAMETER excludedPattern
Parameter description

.EXAMPLE
An example

.NOTES
General notes
#>
Function LookForManifestFile () {
    Param (
        [Parameter(Mandatory = $true)][string]$folderPath,
        [Parameter(Mandatory = $true)][string]$pattern,
        [Parameter(Mandatory = $false)][string]$excludedPattern
    )
    If ((Get-ChildItem $folderPath -Include "$pattern" -Exclude $excludedPattern -Recurse).Count -gt 0) {
        Write-Host "# Step - Manifest file found. Proceeding." -ForegroundColor green
        return $true
    }
    Else {
        Write-Host "# Error - Manifest file not found. Is $folderPath the right manifest folder path?" -ForegroundColor red
        return $false
    }
}

Function CheckIfTeamsAppExists () {
    Param (
        [Parameter(Mandatory = $true)][string]$AppShortName
    )
    $IsSuccessfullyInstalled = Get-PnPTeamsApp -Identity $AppShortName
    If ($IsSuccessfullyInstalled) {
        Write-Host "# Step - The application is installed." -ForegroundColor green
        return $IsSuccessfullyInstalled
    }
    Else {
        Write-Host "# Step - No instance of this application found. " -ForegroundColor green
        return $IsSuccessfullyInstalled
    }
}