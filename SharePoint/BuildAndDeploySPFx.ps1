
###########################################################
# PnP Connection Script 
###########################################################
# Parameters         
# > siteUrl (string)   
# > installApp (bool) => Install or not the app on the site
# > scope (string) => Tenant or Site
###########################################################


## Example command : In the SPFX root folder, type :
## {absolutePathLocation}\BuildAndDeploySPFx.ps1 -siteUrl "https://smeethy.sharepoint.com/sites/Development" -installApp 0 -scope "Tenant"
## This command adds to the tenant app catalog but doesnt install it

# Script command lines - Target Site Collection with an app catalog/do you want to install the app
param(
    [Parameter(Mandatory = $true)][string]$siteUrl,
    [Parameter(Mandatory = $true)][bool]$installApp,
    [Parameter(Mandatory = $true)][string]$scope
)

# External Variables - To get the username
$WorkingDir = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
. $WorkingDir\Helpers\Variables.ps1
. $WorkingDir\Helpers\Functions.ps1

Write-Host " "
Write-Host "########################################"
Write-Host "# --- SPFx Build and Deploy script --- #"
Write-Host "########################################"
Write-Host " "

# 0 - Reading current SPFx project package configuration to prevent known errors
$ReadSPFxSolutionSettings = Get-Content -Raw -Path config\package-solution.json | ConvertFrom-Json

# 1 - Checks if folder exists
Write-Host " "
Write-Host "#### Deployment Folder creation ####"
Write-Host " "
If (Test-Path $GulpFile) {
    CreateFolderIfNotExist $DeploymentLogsFolder "deployment"
}
Else {
    Write-Host "# Error - This folder does not contain a gulpfile. You most likely are not in an SPFx solution. Exiting..." -ForegroundColor red
    exit
}


# 2 - Tries to bundle, if there are errors/warning, Exit code != 0
Write-Host " "
Write-Host "#### Gulp Bundleing ####"
Write-Host " "
Try {
    Write-Host "# Step - Bundling the solution..." -ForegroundColor green
    $OutputBundle = gulp bundle --ship 2>&1
    $OutputBundle >> ./deployment/bundle.log
    If ($LastExitCode -ne 0) {
        throw "# Error - You had errors or warning while building. Cannot proceed to packaging. Please have a look at the logs in deployment folder. Exiting..."
    }
}
Catch [Exception] {
    $ErrorMessage = $_.Exception.Message
    Write-Error "$ErrorMessage"
    exit
}

# 3 - Package the solution
Write-Host " "
Write-Host "#### Packaging Solution ####"
Write-Host " "
Try {
    Write-Host "# Step - Packaging the solution..." -ForegroundColor green
    $OutputPackage = gulp package-solution --ship 2>&1
    $OutputPackage >> ./deployment/package.log
    If ($LastExitCode -ne 0) {
        throw "# Error - You had errors or warning while packaging. The sppkg is not ready. Please have a look at the package logs in deployment folder. Exiting..."
    }
    Elseif ($LastExitCode -eq 0) {
        Write-Host "# Step - App Processing Complete - The solution has been packaged properly." -ForegroundColor green
    }
}
Catch [Exception] {
    $ErrorMessage = $_.Exception.Message
    Write-Error "$ErrorMessage"
    exit
}

# 4 - Connect to the SharePoint site using PnPConnection.ps1
Write-Host " "
Write-Host "#### PnP Connection ####"
Write-Host " "
$CurrentConnectionInstance = . $WorkingDir\Helpers\PnPConnection.ps1 -siteUrl $siteUrl

# 5 - Verifying if this app is already installed on the site + If it is set as Domain Isolated in package-solution (Which may cause error if true)
Write-Host " "
Write-Host "#### Verifying SPFx package config ####"
Write-Host " "

$AppAlreadyInstalled = Get-PnPAppInstance -Identity $ReadSPFxSolutionSettings.solution.name 
$IsAppDomainIsolated = [bool]$ReadSPFxSolutionSettings.solution.isDomainIsolated

If ($IsAppDomainIsolated) {
    Write-Host "# Warning - This SPFx app package-solution.json states it is isDomainIsolated: true. The app may not install properly." -ForegroundColor DarkYellow
}
Else {
    Write-Host "# Info - Make sure you change the version (min/major) in the package-solution for each release. Installing version:" $ReadSPFxSolutionSettings.solution.version -ForegroundColor DarkCyan
}

# 6 - Add the App - Adds it and overwrites if existing
Write-Host " "
Write-Host "#### Adding to App Catalog ####"
Write-Host " "
Try {
    Write-Host "# Step - Adding/Updating the spfx solution to the app catalog ..." -ForegroundColor green
    $AppPath = Resolve-Path "./sharepoint/solution/*.sppkg" | Select -ExpandProperty Path
    $AddedApp = Add-PnPApp -Path $AppPath -Scope $scope -Publish -Overwrite
} 
Catch [Exception] {
    $ErrorMessage = $_.Exception.Message
    Write-Error "# Error - While adding the SPFx app to the catalog: $ErrorMessage"
}

# 7 - Install the app
Write-Host " "
Write-Host "#### Installing the app on the site ####"
Write-Host " "
If ($AppAlreadyInstalled) {
    Write-Host "# Warning - You used the parameter -installApp 1 but the App is already installed on this site. Will not install. Skipping." -ForegroundColor DarkYellow
    Write-Host "# Info - Once it is installed, you simply have to update the app on the catalog." -ForegroundColor DarkCyan
}
Elseif ($installApp -and !$AppAlreadyInstalled) {
    Try {
        Write-Host "# Step - Installing the spfx solution ..." -ForegroundColor green
        Install-PnPApp -Identity $AddedApp."ID" -Scope Site
    }
    Catch [Exception] {
        $ErrorMessage = $_.Exception.Message
        Write-Error "# Error - While installing the SPFx app: $ErrorMessage"
    }
}
Else {
    Write-Host "# Info - Wasnt asked to install. Skipping..." -ForegroundColor DarkCyan
}

# 8 - Closing
Write-Host " "
Write-Host "#### Closing connection ####"
Write-Host " "
Write-Host "# Step - Closing connection ..." -ForegroundColor green
if ($CurrentConnectionInstance) {
    Disconnect-PnPOnline
}
Write-Host " "
Write-Host "# Info - End of script without errors." -ForegroundColor DarkCyan
