######################
# Export List to CSV #
######################
# Parameters         #
######################
# siteUrl (string)   #
# listName (string)  #
# fields (string)    #
######################

# Example
# .\ExportListToCSV.ps1 -siteurl "https://{tenant}.sharepoint.com/sites/Development" -listName "ListName" -fields @("Title","Number")

# Params
param(
    [Parameter(Mandatory = $true)][string]$siteUrl,
    [Parameter(Mandatory = $true)][string]$listName,
    [Parameter(Mandatory = $true)][String[]]$fields    
)

# Import functions
$WorkingDir = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
. $WorkingDir\Helpers\Variables.ps1
. $WorkingDir\Helpers\Functions.ps1

Write-Host "############################################"
Write-Host "# --- Exporting SharePoint List script --- #"
Write-Host "############################################"

# 1 - Creating folder if required
CreateFolderIfNotExist $ExportedCSV "Exported List Csv"

# 2 - Connect to SharePoint site using PnPConnection.ps1
$CurrentConnectionInstance = . $WorkingDir\Helpers\PnPConnection.ps1 -siteUrl $siteUrl

# 3 - Exports the SharePoint list
ExportList -listName $listName -fields $fields

# 4 - Close connection
Write-Host " "
Write-Host "#### Closing connection ####"
Write-Host " "
Write-Host "# Step - Closing connection ..."
If ($CurrentConnectionInstance) {
    Disconnect-PnPOnline
}
Write-Host "# Info - End of script without errors." -ForegroundColor green