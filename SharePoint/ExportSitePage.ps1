######################
# Export Page Template
######################
# Parameters         
######################
# siteUrl (string)   
# pageName (string)  
######################
# Example: ./ExportSitePage.ps1 -siteUrl "tenanturl" -pageName "myPage"
# Params
param(
    [Parameter(Mandatory = $true)][string]$siteUrl,
    [Parameter(Mandatory = $true)][string]$pageName
)

# Import functions
$WorkingDir = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
. $WorkingDir\Helpers\Variables.ps1
. $WorkingDir\Helpers\Functions.ps1

Write-Host "############################################"
Write-Host "# --- Exporting SharePoint Page script --- #"
Write-Host "############################################"

# 1 - Checks if folder exists
CreateFolderIfNotExist $ExportedPages "Exported Client Side Pages"

# 2 - Connect to SharePoint site using PnPConnection.ps1
$CurrentConnectionInstance = . $WorkingDir\Helpers\PnPConnection.ps1 -siteUrl $siteUrl

# 3 - Exports the SharePoint Page
$Page = $pageName + ".aspx" 
$Output = "$ExportedPages\$pageName" + ".pnp"
Export-PnPClientSidePage -Identity $Page -Out $Output

# 4 - Close connection
Write-Host " "
Write-Host "#### Closing connection ####"
Write-Host " "
Write-Host "# Step - Closing connection ..."
if ($CurrentConnectionInstance) {
    Disconnect-PnPOnline
}
Write-Host "# Info - End of script without errors." -ForegroundColor green