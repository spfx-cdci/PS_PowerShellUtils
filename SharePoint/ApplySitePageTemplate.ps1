######################
# Export Page Template
######################
# Parameters         
######################
# siteUrl (string)   
# pageName (string)  
######################

# Params
param(
    [Parameter(Mandatory = $true)][string]$siteUrl,
    [Parameter(Mandatory = $true)][string]$pageName    
)

# Import functions
$WorkingDir = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
. $WorkingDir\Helpers\Variables.ps1
. $WorkingDir\Helpers\Functions.ps1

Write-Host "#########################################################"
Write-Host "# --- Applying SharePoint Site Page Template script --- #"
Write-Host "#########################################################"

# 1 - Checks if folder exists
CreateFolderIfNotExist $ExportedPages "Applying Site Page Template"

# 2 - Connect to SharePoint site using PnPConnection.ps1
$CurrentConnectionInstance = . $WorkingDir\Helpers\PnPConnection.ps1 -siteUrl $siteUrl

# 3 - Exports the SharePoint Page
Try {
    Apply-PnPProvisioningTemplate -Path pageTemplates\Template.xml 
}
Catch [Exception] {
    $ErrorMessage = $_.Exception.Message         
    Write-Host "# Error - Error while applying Site Page PnP Template: $ErrorMessage" -ForegroundColor Red        
}

# 4 - Close connection
Write-Host "# Step - Closing connection ..." -ForegroundColor green
If ($CurrentConnectionInstance) {
    Disconnect-PnPOnline
}
Write-Host "# Info - End of script without errors." -ForegroundColor green