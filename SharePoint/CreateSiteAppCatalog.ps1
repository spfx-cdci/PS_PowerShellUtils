
###########################################################
# App Catalog creation
###########################################################
# Parameters         
# > siteUrl (string)   
###########################################################
# Example 
# ./CreateSiteAppCatalog.ps1 -siteUrl [SPUrl]
###########################################################

param(
    [Parameter(Mandatory = $true)][string]$siteUrl
    )

# External Variables - To get the username
$WorkingDir = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
. $WorkingDir\Helpers\Variables.ps1

Write-Host "###########################################"
Write-Host "# --- App Catalog Provisioning script --- #"
Write-Host "###########################################"

# 1 - Checks if the PnP Module is installed ----
If (Get-Module -ListAvailable -Name "SharePointPnPPowerShell*") {
    Write-Host "# Step - A PnP Powershell module was found. Processing." -ForegroundColor green
}
Else {
    Write-Error "# Error - No PnP Powershell module was found. Exiting."
    exit
}

# 2 - Read Parameters ----
If ($siteUrl) {
    Write-Host "Parameters passed:" -ForegroundColor green
    Write-Host "SiteCollectionUrl: $siteUrl"
}
Else {
    Write-Error "# Error - You did not enter any site Url. Exiting."
}

# 3 - Connecting to the SharePoint site
Try {
    $CurrentConnectionInstance = . $WorkingDir\Helpers\PnPConnection.ps1 -siteUrl $siteUrl
}
Catch [Exception] {
    $ErrorMessage = $_.Exception.Message
    Write-Error "# Error - Connection failed due to the following reason: $ErrorMessage"
}

# 4 - Checks if it already exists
$AppCatalogList = Get-PnPList -Identity "AppCatalog"
If ($AppCatalogList) {
    Write-Host "# Error - This site already has an app catalog. Exiting." -ForegroundColor red
    exit
}
Else {
    Write-Host "# Step - This Site has no app catalog. Creating." -ForegroundColor green

    # 5 - Creating the app catalog
    Try {
        Add-PnPSiteCollectionAppCatalog -Site $siteUrl
    }
    Catch [Exception] {
        $ErrorMessage = $_.Exception.Message
        Write-Error "# Error - App Catalog creation failed due to the following reason: $ErrorMessage"
        Write-Information "# Info - Additionnally, make sure you already have a tenant app catalog as it is a prerequesite."
    }
}

# 6 - Closing
Write-Host " "
Write-Host "#### Closing connection ####"
Write-Host " "
Write-Host "# Step - Closing connection ..."
If ($CurrentConnectionInstance) {
    Disconnect-PnPOnline
}
Write-Host "# Info - End of script without errors." -ForegroundColor green
