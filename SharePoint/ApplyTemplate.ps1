######################
# Apply any pnp template 
######################
# Parameters         
######################
# siteUrl (string)   
# xmlFileName (string)  
######################
# Example command:  .\ApplyTemplate.ps1 -siteUrl "https://tenant.sharepoint.com/sites/TestSite" -xmlFileName "ListSample"
# GOTCHAS: Launching the script again updates the list structure and so far for text fields, it doesnt affect data in other fields (To test with exotic fields)

# Params
param(
    [Parameter(Mandatory = $true)][string]$siteUrl,
    [Parameter(Mandatory = $true)][string]$xmlFileName
)

# Import functions
$WorkingDir = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
. $WorkingDir\Helpers\Variables.ps1
. $WorkingDir\Helpers\Functions.ps1

Write-Host "###############################"
Write-Host "# --- Apply PnP Templates --- #"
Write-Host "###############################"

# 1 - Checks if folder exists, if not, creates and asks to retry
Write-Host " "
Write-Host "#### Checking if the folder exists ####"
Write-Host " "
CreateFolderIfNotExist $XMLTemplatesFolder "SP List XML templates"
$DirectoryInfo = Get-ChildItem $XMLTemplatesFolder | Measure-Object

If ($DirectoryInfo.count -eq 0) {
    Write-Host "# Exiting - The folder is empty. Most likely because the script previously created it because there was no folder. Add the XML files and retry." -ForegroundColor Blue
}

# 2 - Checks if the xml file exists
Write-Host " "
Write-Host "#### Finding the xml file ####"
Write-Host " "
$XMLTemplate = "$XMLTemplatesFolder\$xmlFileName" + ".xml"
If (Test-Path $XMLTemplate) {
    Write-Host "# Step - XML file named $xmlFileName was found." -ForegroundColor green
}
Else {
    Write-Host "# Exiting - XML file named $xmlFileName was not found." -ForegroundColor Red
    exit
}

# 3 - Connect to SharePoint site using PnPConnection.ps1
Write-Host " "
Write-Host "#### Connecting to SharePoint ####"
Write-Host " "
$CurrentConnectionInstance = . $WorkingDir\Helpers\PnPConnection.ps1 -siteUrl $siteUrl

# 4 - Apply the template
Write-Host " "
Write-Host "#### Applying the XML template ####"
Write-Host " "
Try {
    Apply-PnPProvisioningTemplate -Path $XMLTemplate
    Write-Host "# Step - Template Applied." -ForegroundColor green
}
Catch [Exception] {
    $ErrorMessage = $_.Exception.Message
    Write-Error "$ErrorMessage"
    exit
}

# 5 - Close connection
Write-Host " "
Write-Host "#### Closing connection ####"
Write-Host " "
Write-Host "# Step - Closing connection ..."
if ($CurrentConnectionInstance) {
    Disconnect-PnPOnline
}
Write-Host "# Info - End of script without errors." -ForegroundColor green